fn inc(n: i32) -> i32 {
    n + 1
}

fn abs(n: i32) -> i32 {
    if n >= 0 {
        n
    } else {
        n - 1
    }
}

fn main() {
    let t = inc(1);

    println!("t = {}\nabs(-2) = {}", t, abs(-2));

    for element in [1, 42].iter() {
        println!("{}", element)
    }

    for elem in (0..4).rev() {
        println!("elem = {}", elem)
    }
}
