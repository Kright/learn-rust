use std::sync::Arc;
use std::sync::mpsc;
use std::sync::Mutex;
use std::thread;
use std::time::Duration;

fn main() {
    main1();
    main2();


    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();
            *num += 1;
        });
        handles.push(handle)
    }

    for h in handles {
        h.join().unwrap();
    }

    println!("counter = {:?}", counter);
}

fn main1() {
    let iterations_count = 10;

    let handle = thread::spawn(move || {
        for i in 1..iterations_count {
            println!("hi {} form spawned thread", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    for i in 1..5 {
        println!("hi {} form main thread", i);
        thread::sleep(Duration::from_millis(1));
    }

    println!("joining!");

    handle.join().unwrap();

    println!("main1 finished!");
}

fn main2() {
    let (tx, rx) = mpsc::channel();

    let tx2 = tx.clone();

    thread::spawn(move || {
        let vals: Vec<_> = vec![
            "the",
            "long",
            "sequence",
        ].iter().map(|&s| String::from(s)).collect();

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_millis(1));
        };
    });


    thread::spawn(move || {
        let vals: Vec<_> = vec![
            "another",
            "trivial",
            "vec",
        ].iter().map(|&s| String::from(s)).collect();

        for val in vals {
            tx2.send(val).unwrap();
            thread::sleep(Duration::from_millis(1));
        };
    });

    for received in rx {
        println!("received: {}", received);
    }
    println!("main2 finished!");
}
