fn main() {
    let inc = |i| i + 1;

    println!("inc(2) = {}", inc(2));

    let x = 2;

    let mut lazy = Cached::new(|| {
        println!("eval");
        x + x
    });

    println!("kek");
    println!("vazy = {}", lazy.value());

    let v1 = vec![1, 2, 3];
    let v1_iter = v1.iter();
    for i in v1_iter.filter(|&x| x % 2 == 1) {
        println!("{}", *i);
    }

    let v2: Vec<_> = v1.iter().map(|x| x + 1).collect();
    println!("{:?}", v2);

    for i in Counter::new() {
        println!("i = {}", i);
    }
}

struct Cached<F>
    where F: Fn() -> u32
{
    calculation: F,
    value: Option<u32>,
}

impl<F> Cached<F>
    where F: Fn() -> u32
{
    fn new(calculation: F) -> Cached<F> {
        Cached {
            calculation,
            value: None,
        }
    }

    fn value(&mut self) -> u32 {
        match self.value {
            Some(v) => v,
            None => {
                let v = (self.calculation)();
                self.value = Some(v);
                v
            }
        }
    }
}

struct Counter {
    count: u32,
}

impl Counter {
    fn new() -> Counter {
        Counter { count: 0 }
    }
}

impl Iterator for Counter {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        self.count +=1;

        if self.count < 6 {
            Some(self.count)
        } else {
            None
        }
    }
}
