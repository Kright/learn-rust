use std::collections::HashMap;

fn main() {
    let mut v: Vec<i32> = Vec::new();

    v.push(0);
    v.push(1);
    v[0] = -1;

    for i in &mut v {
        *i += 1
    }

    println!("v = {:?}", v);

    let v = vec!(1, 2, 3);
    println!("v = {:?}", v);

    let third = &v[2];
    println!("v[2] == {}, v.get(2) == {:?}", third, v.get(2));

    for i in &v {
        println!("{}", i);
    }

    enum Value {
        Int(i32),
        Float(f32),
    }

    let values = vec![Value::Int(1), Value::Float(1.0)];

    let mut map = HashMap::new();

    map.insert(String::from("key"), 1);
    map.insert(String::from("xz"), 42);

    println!("{:?}", map);

    {
        let keys = ["k1", "k2"].iter().map(|&s| { String::from(s) }).collect::<Vec<_>>();
        let vals = vec![1, 2];
        let map: HashMap<_, _> = keys.iter().zip(vals.iter()).collect();
        println!("{:?}", map);

        for (key, value) in &map {
            println!("{}: {}", key, value);
        }
    }

    {
        let mut v = vec![1, 2, 3, 4, 10];
        println!("{} {} {}", v.mean(), v.median(), v.mode());
    }

    {
        println!("{}", to_pig_latin(String::from("Hello world  ")));
    }
}

trait MyExt {
    fn mean(&self) -> f64;

    fn median(&mut self) -> i32;

    fn mode(&self) -> i32;
}

impl MyExt for Vec<i32> {
    fn mean(&self) -> f64 {
        let mut sum: i64 = 0;
        self.iter().for_each(|&i| { sum += i as i64 });
        return sum as f64 / self.len() as f64;
    }

    fn median(&mut self) -> i32 {
        self.sort();
        self[self.len() / 2]
    }

    fn mode(&self) -> i32 {
        let mut map: HashMap<i32, i32, _> = HashMap::new();
        for &i in self {
            if map.contains_key(&i) {
                map.insert(i, map[&i] + 1);
            } else {
                map.insert(i, 0);
            }
        }

        let (mut result, mut count) = (0, 0);
        for (key, value) in map {
            if value > count {
                result = key;
                count = value;
            }
        }
        return result;
    }
}

fn word_to_pig_latin(s: &str) -> String {
    let mut result = String::from("");

    let mut first: Option<char> = None;
    for c in s.chars() {
        if first.is_none() {
            first = Some(c);
        } else {
            result.push(c);
        }
    }

    if let Some(c) = first {
        result.push('-');
        result.push(c);
        result.push_str("ay");
    }

    result
}

fn to_pig_latin(s: String) -> String {
    let mut result = String::from("");

    for word in s.split(" ") {
        println!("{}", word);
        result.push_str(&(word_to_pig_latin(word) + " "));
    }

    result
}
