use add_one;

fn main() {
    println!("add_one(2) = {}", add_one::add_one(2));
}
