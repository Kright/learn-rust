pub fn add_one(i: i32) -> i32 {
    i + 1
}

#[cfg(test)]
mod tests {
    use rand;

    #[test]
    fn it_works() {
        assert_eq!(super::add_one(2), 3);
    }

    #[test]
    fn it_works_on_rnd() {
        let i: i32 = rand::random();
        assert_eq!(super::add_one(i), i + 1);
    }
}
