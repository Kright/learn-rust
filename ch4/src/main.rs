fn main() {
    let mut s = String::from("hello");

    s.replace_range(0..1, "H");
    s.push_str(", world!");

    let s2 = s;
    let s3 = s2.clone();

    println!("{}", s2);
    println!("{}", s3);

    {
        let s = make_string();
        let mut s = consume_and_return(s);
        addx3(&mut s);
        println!("len = {}", get_len(&s));
        consume(s);
    }

    {
        let s = "Hello world!";

        let hello = &s[0..5];
        let world = &s[6..11];
    }
}

fn make_string() -> String {
    String::from("string")
}

fn consume_and_return(mut s: String) -> String {
    s.push_str("x2");
    s
}

fn consume(s: String) {
    println!("s = {}", s);
}

fn get_len(s: &String) -> usize {
    s.len()
}

fn addx3(s: &mut String) {
    s.push_str("x3")
}
