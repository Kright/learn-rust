use std::fmt;
use std::fmt::Display;

fn main() {
    let lst = vec![1, 2, 3, 4];

    println!("{}", largest(&lst));

    let point2i = Point { x: 5, y: 10 };
    let point2f: Point<f32> = Point { x: 5.0, y: 10.0 };

    println!("{}, dist = {}", point2f, point2f.distance_from_origin());

    println!("{}", crate::reduce(&[String::from("Hello"), String::from("world")]));

    println!("{}", double(String::from("kek")));
    println!("{}", quarter(String::from("kek")));

    {
        let s1 = "s1";
        let s2 = String::from("r2");
        let l = longest_str(s1, &s2);
        println!("{}", l);
    }

    {
        let s:&'static str = "str";
        let h = RefHolder{
            reference: s
        };
        println!("{}", h.reference);
    }
}

fn longest_str<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

struct RefHolder<'a> {
    reference: &'a str,
}

fn largest<T: PartialOrd>(list: &[T]) -> &T {
    let mut largest = &list[0];

    for item in list.iter() {
        if *item > *largest {
            largest = item;
        }
    }

    largest
}

fn reduce<T: Monoid<T>>(list: &[T]) -> T {
    list.iter().fold(T::empty(), T::combine)
}

fn double<T: Clone + Monoid<T>>(item: T) -> T {
    let right = item.clone();
    item.combine(&right)
}

fn quarter<T>(t: T) -> T
    where T: Clone + Monoid<T> {
    double(double(t))
}

#[derive(Debug)]
struct Point<T> {
    x: T,
    y: T,
}

impl<T: Display> fmt::Display for Point<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Point({}, {})", self.x, self.y)
    }
}

impl<T> Point<T> {
    fn x(&self) -> &T {
        &self.x
    }

    fn y(&self) -> &T {
        &self.y
    }
}

impl Point<f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}

pub trait Monoid<T> {
    fn combine(self, other: &T) -> T;

    fn empty() -> T;
}

//mod concatenation {
//    impl crate::Monoid<String> for String {
//        fn combine(self, other: &String) -> String {
//            self + other
//        }
//
//        fn empty() -> String {
//            String::new()
//        }
//    }
//}
// several implementations are impossible in Rust :(

impl Monoid<String> for String {
    fn combine(self, other: &String) -> String {
        if self.is_empty() { return String::from(other); };
        if other.is_empty() { return self; }
        return self + " " + other;
    }

    fn empty() -> String {
        String::new()
    }
}

