#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn my_test() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    #[should_panic]
    fn another() {
        println!("{}", abs_i8(-128));
    }

    #[test]
    fn test_add() {
        assert_eq!(add_two(2), 4);
        assert_eq!(add_two(0), 2);
    }

    #[test]
    fn my_test_r() -> Result<(), String> {
        if abs_i8(1) == 1 {
            Ok(())
        } else {
            Err(String::from("abs(1)!=1"))
        }
    }
}

pub fn add_two(a: i32) -> i32 {
    a + 2
}

pub fn abs_i8(a: i8) -> i8 {
    if a >= 0 {
        a
    } else {
        -a
    }
}
