fn print_pair(&(a, b): &(i32, i32)) {
    println!("({}, {})", a, b);
}

fn main() {
    let a = (1, 2);
    print_pair(&a);
    print_pair(&(2, 3));
}
