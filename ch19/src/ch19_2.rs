use std::mem;

use super::Point2i;

trait Pilot {
    fn fly(&self);
}

trait Wizard {
    fn fly(&self);
}

struct Human;

impl Pilot for Human {
    fn fly(&self) {
        println!("This is your captain speaking.");
    }
}

impl Wizard for Human {
    fn fly(&self) {
        println!("Up!");
    }
}

impl Human {
    fn fly(&self) {
        println!("*waving arms furiously*");
    }
}

pub fn main() {
    let mut p = Point2i::new(1, 2);
    p = p + p;
    p = p * 2 * p;
    println!("{:?}", p);

    let person = Human;
    person.fly();
    Pilot::fly(&person);

    println!("int {}", mem::size_of::<i32>());
    println!("Box<int> {}", mem::size_of::<Box<i32>>());
    println!("Option<Box<int>> {}", mem::size_of::<Option<Box<i32>>>());
    println!("bool {}", mem::size_of::<bool>());
    println!("Option<bool>> {}", mem::size_of::<Option<bool>>());
    println!("i8 {}", mem::size_of::<i8>());
    println!("Option<i8> {}", mem::size_of::<Option<i8>>());
}