use std::slice;

pub fn main() {
    println!("{}", HELLO_WORLD);

    let mut arr = [1, 2, 3, 4, 5];

    let (left, right) = split_at_mut(&mut arr, 3);
    println!("left = {:?}, right = {:?}", left, right);

    unsafe {
        dangerous();
        println!("abs(-3) = {}", abs(-3));
    }

    add_to_count(3);
    unsafe {
        println!("COUNTER = {}", COUNTER);
    }
}


unsafe fn dangerous() {
    println!("kek");
}

fn split_at_mut(slice: &mut [i32], mid: usize) -> (&mut [i32], &mut [i32]) {
    let len = slice.len();
    let ptr = slice.as_mut_ptr();
    assert!(mid <= len);
    unsafe {
        (slice::from_raw_parts_mut(ptr, mid),
         slice::from_raw_parts_mut(ptr.offset(mid as isize), len - mid))
    }
}

extern "C" {
    fn abs(input: i32) -> i32;
}

#[no_mangle]
pub extern "C" fn call_from_c() {
    println!("Rust func was called!");
}

static HELLO_WORLD: &str = "Hello world";
static mut COUNTER: u32 = 0;

fn add_to_count(inc: u32) {
    unsafe {
        COUNTER += inc
    }
}