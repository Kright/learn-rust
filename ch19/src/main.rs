mod ch19_2;
mod ch19_1;
mod ch19_5;
mod point2i;

use point2i::Point2i;

fn main() {
    ch19_1::main();
    ch19_2::main();
    Point2i::new(1, 2).outline_print();

    ch19_5::main();
}


use std::fmt;


pub trait OutlinePrint: fmt::Display {
    fn outline_print(&self) {
        let output = self.to_string();
        let len = output.len();
        println!("{}", "*".repeat(len + 4));
        println!("*{}*", " ".repeat(len + 2));
        println!("* {} *", output);
        println!("*{}*", " ".repeat(len + 2));
        println!("{}", "*".repeat(len + 4));
    }
}

