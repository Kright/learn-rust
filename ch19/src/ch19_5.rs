macro_rules! my_vec {
    ( $( $x:expr ), *) => {
        {
            let mut temp_vec = Vec::new();
            $(
                temp_vec.push($x);
            )*
            temp_vec
        }
    };
}

pub fn main() {
    let v: Vec<u32> = my_vec![1, 2, 3];

    println!("{:?}", v);
}