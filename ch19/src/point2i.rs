use std::fmt;
use std::ops::Add;
use std::ops::Mul;

#[derive(Copy, Clone, Debug)]
pub struct Point2i {
    pub x: i32,
    pub y: i32,
}

impl Point2i {
    pub fn new(x: i32, y: i32) -> Point2i {
        Point2i { x, y }
    }
}

impl super::OutlinePrint for Point2i {}

impl fmt::Display for Point2i {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Point2i({}, {})", self.x, self.y)
    }
}

impl Add<Point2i> for Point2i {
    type Output = Point2i;

    fn add(self, rhs: Point2i) -> Self::Output {
        Point2i::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl Mul<Point2i> for Point2i {
    type Output = Self;

    fn mul(self, rhs: Point2i) -> Self::Output {
        Self::new(self.x * rhs.x, self.y * rhs.y)
    }
}

impl Mul<i32> for Point2i {
    type Output = Self;

    fn mul(self, rhs: i32) -> Self::Output {
        Self::new(self.x * rhs, self.y * rhs)
    }
}