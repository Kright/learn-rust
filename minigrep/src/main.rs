//! # Minigrep
//!
//! very simple implementation of grep

use std::env;
use std::process;

use minigrep::Config;

fn main() {
    let config = Config::parse(env::args()).unwrap_or_else(|err| {
        eprintln!("can't parse arguments: {}", err);
        process::exit(1);
    });

    let result = minigrep::run(config);
    if let Err(err) = result {
        eprintln!("error: {}", err);
        process::exit(1);
    }
}

