use std::env;
use std::error::Error;
use std::fs;
use std::vec::Vec;

pub struct Config {
    pub query: String,
    pub filename: String,
    pub case_sensitive: bool,
}

impl Config {
    // no overloading in rust :(
    pub fn parse(mut args: std::env::Args) -> Result<Config, &'static str> {
        args.next();

        let query = match args.next() {
            Some(arg) => arg,
            None => return Err("no query specified"),
        };

        let filename = match args.next() {
            Some(arg) => arg,
            None => return Err("no filename specified"),
        };
        let case_sensitive = env::var("CASE_INSENSITIVE").is_err();

        Ok(Config { query, filename, case_sensitive })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let text = fs::read_to_string(config.filename)?;

    let func = if config.case_sensitive {
        search
    } else {
        search_case_insensitive
    };

    for line in func(&config.query, &text) {
        println!("{}", line);
    }

    Ok(())
}

///
/// # Examples
/// ```
/// let answer = minigrep::search("aa", "aaaa\nbb");
/// assert_eq!(answer, vec!["aaaa"]);
/// ```
pub fn search<'a>(query: &str, text: &'a str) -> Vec<&'a str> {
    text.lines()
        .filter(|line| line.contains(query))
        .collect()
}

fn search_case_insensitive<'a>(query: &str, text: &'a str) -> Vec<&'a str> {
    let query = query.to_lowercase();
    let mut vec = Vec::new();

    for line in text.lines() {
        if line.to_lowercase().contains(&query) {
            vec.push(line);
        }
    }

    vec
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn case_sensitive() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(
            vec!["safe, fast, productive."],
            search(query, contents)
        )
    }

    #[test]
    fn case_insensitive() {
        let query = "rUsT";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";

        assert_eq!(
            vec!["Rust:", "Trust me."],
            search_case_insensitive(query, contents)
        );
    }
}