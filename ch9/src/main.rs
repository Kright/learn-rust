use std::error::Error;
use std::fs::File;
use std::io::ErrorKind;
use std::io::Read;

fn main() -> Result<(), Box<dyn Error>> {
    let v = vec![1, 2, 3];

    /*
    let f = File::open("src/main.rs");


    let f = match f {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => panic!("file not found: {}", error),
            other_error => panic!("other error {}", error),
        }
    };
    */

    // let f = f.unwrap_or_else(|error| { panic!(error) });
    // let f = f.unwrap();

    /*
    let mut f = File::open("src/main.rs").expect("failed ot open file");

    let mut text = String::new();


    match f.read_to_string(&mut text) {
        Ok(size) => println!("content len = {}:\n{}", size, text),
        Err(e) => panic!(e),
    };
    */

    let text = read_file("src/main.rs").unwrap();
    let text2 = std::fs::read_to_string("src/main.rs")?;
    assert_eq!(text, text2);
    println!("file content:\n{}", text);
    Ok(());
}

fn read_file(file_name: &str) -> Result<String, std::io::Error> {
    let mut text = String::new();
    File::open(file_name)?.read_to_string(&mut text);
    Ok(text)
}
