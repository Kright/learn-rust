use std::ops::Deref;
use std::rc::Rc;

fn main() {
    let b = Box::new(5);

    println!("{}", b);

    {
        use crate::List::{Cons, Nil};

        let lst = Cons(2, Box::new(Cons(1, Box::new(Nil))));
        println!("{:?}", lst);

        println!("sum = {:?}", lst.fold_left(0, |a, b| { a + b }));
    }

    let x = 5;
    let y = MyBox::new(x);
    assert_eq!(x, *y);

    let z = MyBox::new(MyBox::new(x));
    assert_eq!(x, **z);

    assert_eq!(6, next(&y));

    println!("{}", next(&z));
    drop(z);

    {
        use crate::RcList::{Cons, Nil};

        let a = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
        {
            let b = Cons(3, Rc::clone(&a));
            let c = Cons(4, Rc::clone(&a));
            println!("{}", Rc::strong_count(&a));
        }
        println!("{}", Rc::strong_count(&a));
    }
}

fn next(a: &i32) -> i32 {
    a + 1
}

#[derive(Debug)]
enum List {
    Cons(i32, Box<List>),
    Nil,
}

impl List {
    fn fold_left<Acc, F>(&self, initial: Acc, func: F) -> Acc
        where F: Fn(Acc, &i32) -> Acc
    {
        match self {
            List::Nil => initial,
            List::Cons(value, tail) => tail.fold_left(func(initial, value), func),
        }
    }

    fn tail(&self) -> Option<&List> {
        match self {
            List::Nil => None,
            List::Cons(_, tail) => Some(&tail),
        }
    }
}

enum RcList {
    Cons(i32, Rc<RcList>),
    Nil,
}

#[derive(Debug)]
struct MyBox<T>(T);

impl<T> MyBox<T> {
    fn new(x: T) -> MyBox<T> {
        MyBox(x)
    }
}

impl<T> Deref for MyBox<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}

impl<T> Drop for MyBox<T> {
    fn drop(&mut self) {
        println!("drop MyBox");
    }
}
