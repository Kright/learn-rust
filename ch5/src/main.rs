use std::ops::Add;

#[derive(Debug, Copy, Clone)]
struct Point2d {
    x: i32,
    y: i32,
}

impl Point2d {
    fn new(x: i32, y: i32) -> Point2d {
        Point2d { x, y }
    }
}

impl Add for Point2d {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self::new(self.x + other.x, self.y + other.y)
    }
}

#[derive(Debug, Copy, Clone)]
struct Rectangle2d {
    pos: Point2d,
    size: Point2d,
}

impl Rectangle2d {
    fn area(&self) -> i32 {
        self.size.x * self.size.y
    }

    fn max_x(&self) -> i32 {
        self.pos.x + self.size.x
    }

    fn max_y(&self) -> i32 {
        self.pos.y + self.size.y
    }

    fn contains(&self, rect: &Rectangle2d) -> bool {
        self.pos.x <= rect.pos.x &&
            self.pos.y <= rect.pos.y &&
            self.max_x() >= rect.max_x() &&
            self.max_y() >= rect.max_y()
    }

    fn destroy(self) {}
}

struct Color(u8, u8, u8);

fn main() {
    let mut p = Point2d {
        x: 2,
        y: 3,
    };

    p.y += 2;

    let p = Point2d {
        x: p.x,
        ..p
    };

    let black = Color(0, 0, 0);

    let rect = Rectangle2d {
        pos: Point2d::new(0, 0),
        size: Point2d::new(3, 4),
    };

    let p = p + p;

    println!("x = {}, y = {}", p.x, p.y);
    println!("p = {:?}", p);
    println!("{:?}", rect);
    println!("area = {}", rect.area());

    rect.destroy();
}
