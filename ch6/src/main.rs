#[derive(Debug)]
enum IpVersion {
    V4(u8, u8, u8, u8),
    V6(String),
}

fn my_print(ipv: &IpVersion) {
    match ipv {
        IpVersion::V4(a, b, c, d) => println!("Ipv4({}, {}, {}, {})", a, b, c, d),
        IpVersion::V6(string) => println!("Ipv6({})", string)
    }
}

fn main() {
    let mut a = IpVersion::V4(192, 168, 1, 1);
    a = IpVersion::V6(String::from("192.168.1.1"));

    println!("a = {:?}", a);
    my_print(&a);

    let a = Some(32);
    let a = a.map(|x| x + 1);
    println!("a = {:?}", a);

    match a {
        None => println!("None"),
        Some(i) => println!("Some({})", i),
    }

    if let Some(33) = a {
        println!("a == Some(33)");
    }

    for xz in &[2, 3] {
        match xz {
            0 => println!("zero"),
            1 => println!("one"),
            2 => println!("two"),
            _ => println!("unknown")
        }
    }

    {
        let a = Some(1);
        let b = Some(1);

        println!("{}", a == b);
        println!("{}", a.is_some());
        println!("{:?}", a.or(b));
        println!("{}", a <= b);
        println!("{}", a < b);
        println!("{}", Some(2) > Some(1));
        println!("{}", Some(2) > None);
        println!("{}", Some(2) < None);
        println!("{}", Some(-2) > None);
        println!("{}", Some(2).ge(&None));
    }
}
